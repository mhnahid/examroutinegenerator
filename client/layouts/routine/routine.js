/**
 * Created by Marvellous on 25-Mar-17.
 */
import { Template } from 'meteor/templating';
import { Subjects,Exams,Holidays } from '../../../models/models'

import './routine.html';
Template.routineTemplate.onCreated(function () {
    console.log("Hello at routine")
     hDayList = Holidays.find({}).fetch();
});

Template.routineTemplate.helpers({
    'change #examId'(event,instance){
        console.log($('#examId').val());
    },
    examList() {
      return Exams.find({});
    },
    hdayList(){
        return Holidays.find({});
    },
    subjList(){
        return Subjects.find({});
    },
});

Template.routineTemplate.events({
    'submit #selectExam'(event,instance){
        event.preventDefault();
        let examId = $('#examId').val();

        var exam =  Exams.find({_id:examId}).fetch();
        var subjectDb =  Subjects.find({}).fetch();
        var holidayDb =  Holidays.find({}).fetch();

        var startDate="";
        var endDate="";
        var subCounter=0;
        var holidayArray=[];
        var subjectArray=[];
        var routineArray=[];

        exam.forEach(function (ex) {
            startDate=ex.startDate;
            endDate=ex.endDate;
            console.log('at each '+ex.title +' ' +startDate+ ' '+endDate);
        });

        subjectDb.forEach(function (sub) {
            subjectArray.push(sub.title);
        });

        holidayDb.forEach(function (hold) {
            holidayArray.push(hold.hdate);
            console.log('at each '+hold.hdate);
        });


        var start = new Date(startDate);
        var end = new Date(endDate);

        while(start <= end){
            var mm = ((start.getMonth()+1)>=10)?(start.getMonth()+1):'0'+(start.getMonth()+1);
            var dd = ((start.getDate())>=10)? (start.getDate()) : '0' + (start.getDate());
            var yyyy = start.getFullYear();
            var date = yyyy+'-'+mm+'-'+dd;
            console.log(date);
            var flag =true;

            for(var i=0; i<holidayArray.length;i++)
            {
                console.log(holidayArray.length+''+date + ' '+holidayArray[i]+' '+(date==holidayArray[i]));
                if(date==holidayArray[i]){
                    flag=false;
                    break;
                }else{
                    flag=true;
                }
            }

            if(flag==true && subCounter < subjectArray.length){
                var slot1='X';
                var slot2='X';

                if(start.getDate()%2==0){
                    slot1=subjectArray[subCounter];
                }else{
                    slot2=subjectArray[subCounter];
                }

                routineArray.push({'date':date,'slot1':slot1,'slot2':slot2});
                subCounter++;
            }else{
                routineArray.push({'date':date,'slot1':'X','slot2':'X'});
            }

            //******************
            var newDate = start.setDate(start.getDate() + 1);
            start = new Date(newDate);
        }
       routineArray.forEach(function (rot) {
           console.log(''+rot.date+' '+rot.slot1+' '+rot.slot2);
       })

        var rowCount =$('#routineTable tr').length;
        console.log('rowcount '+rowCount);
        if(rowCount>2){
            for(var r=2;r<rowCount;r++)
                $('#routineTable tr:last').remove();
        }
        for(var t=0;t<routineArray.length;t++){
            var trow = "<tr><td>"+routineArray[t].date+"</td><td>"+routineArray[t].slot1+"</td><td>"+routineArray[t].slot2+"</td></tr>tr>";
            $('#routineTable').append(trow);
        }



        console.log("Subject submited "+examId);
    },
    'click .btn-danger'(event, instance){
        console.log(this._id + " deleted");
        Subjects.remove(this._id);

    },
    'click #btnPdfGenerate'(event, instance){
        let  doc = new jsPDF();
        doc.fromHTML($('#pdfRoutine').get(0),20,20,{'width':175});
        doc.save("examRoutine.pdf");
    },


});
