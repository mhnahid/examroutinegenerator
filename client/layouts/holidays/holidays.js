/**
 * Created by Marvellous on 25-Mar-17.
 */
import { Template } from 'meteor/templating';
import { Holidays } from '../../../models/models'

import './holidays.html';
Template.holidaysTemplate.helpers({
    holidayList() {
      return Holidays.find({}, {sort: {hdate: 1}});;
    },
});

Template.holidaysTemplate.events({
    'submit #addHolidays'(event,instance){
        event.preventDefault();
        let hdate =$('#hdate').val();
        let holiday = {
            'hdate':hdate
        }
        Holidays.insert(holiday);
        console.log("holiday submited");
        $('#hdate').val(null);
    },
    'click .btn-danger'(event, instance){
        console.log(this._id + " deleted");
        Holidays.remove(this._id);

    },

});
