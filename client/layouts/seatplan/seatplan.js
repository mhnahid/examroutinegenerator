/**
 * Created by Marvellous on 25-Mar-17.
 */
import { Template } from 'meteor/templating';

import './seatplan.html';


Template.seatPlanTemplate.helpers({
    seatPlan(){
        let seats=[];
        i=0;
        while(i<20){
            let class1 = Math.floor((Math.random() * 3) + 1);
            let class2 = Math.floor((Math.random() * 3) + 1);
            let class3 = Math.floor((Math.random() * 3) + 1);
            if(class1==class2 || class2==class3 || class3==class1){
                continue;
            }else{
                console.log(class1+'   '+class2+'  '+class3);
                row ={'seat1':class1,'seat2':class2,'seat3':class3};
                seats.push(row);
                i++;
            }
        }
        return seats;
    },

});

Template.seatPlanTemplate.events({
    'click #btnGenerate'(event, instance){
        document.location.reload(true);
    },
    'click #btnPdfGenerate'(event, instance){
        let  doc = new jsPDF();
        doc.fromHTML($('#pdfSeatplan').get(0),20,20,{'width':175});
        doc.save("seatPlan.pdf");
    },

});
