/**
 * Created by Marvellous on 25-Mar-17.
 */
import { Template } from 'meteor/templating';
import { Exams } from '../../../models/models'

import './exams.html';
Template.examsTemplate.helpers({
    examList() {
      return Exams.find({});
    },
});

Template.examsTemplate.events({
    'submit #addExam'(event,instance){
        event.preventDefault();
        let title =$('#title').val();
        let startDate =$('#startDate').val();
        let endDate =$('#endDate').val();

        let exam = {
            'title':title,
            'startDate':startDate,
            'endDate':endDate,
            'createdAt':new Date()
        }
        Exams.insert(exam);
        console.log("Exam submited");
        $('#title').val(null);
        $('#startDate').val(null);
        $('#endDate').val(null);
    },
    'click .btn-danger'(event, instance){
        console.log(this._id + " deleted");
        Exams.remove(this._id);

    },

});
