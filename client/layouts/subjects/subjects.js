/**
 * Created by Marvellous on 25-Mar-17.
 */
import { Template } from 'meteor/templating';
import { Subjects } from '../../../models/models'

import './subjects.html';
Template.subjectsTemplate.helpers({
    subjectList() {
      return Subjects.find({});
    },
});

Template.subjectsTemplate.events({
    'submit #addSubject'(event,instance){
        event.preventDefault();
        let title =$('#title').val();
        let subject = {
            'title':title,
            'createdAt':new Date()
        }
        Subjects.insert(subject);
        console.log("Subject submited");
        $('#title').val(null);
    },
    'click .btn-danger'(event, instance){
        console.log(this._id + " deleted");
        Subjects.remove(this._id);

    },

});
