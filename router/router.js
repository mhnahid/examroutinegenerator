/**
 * Created by Marvellous on 25-Mar-17.
 */
FlowRouter.route('/',{
    name:'home',
    action(){
        console.log("at router");
        //BlazeLayout.render('baselayout',{content:'tasks'})
    }
});

FlowRouter.route('/subjects',{
    name:'subject',
    action(){
        BlazeLayout.render('baselayout',{content:'subjectsTemplate'})
    }
});

FlowRouter.route('/exams',{
    name:'exam',
    action(){
        BlazeLayout.render('baselayout',{content:'examsTemplate'})
    }
});

FlowRouter.route('/holidays',{
    name:'holidays',
    action(){
        BlazeLayout.render('baselayout',{content:'holidaysTemplate'})
    }
});

FlowRouter.route('/routine',{
    name:'routine',
    action(){
        BlazeLayout.render('baselayout',{content:'routineTemplate'})
    }
});

FlowRouter.route('/seatplan',{
    name:'seatplan',
    action(){
        BlazeLayout.render('baselayout',{content:'seatPlanTemplate'})
    }
});

FlowRouter.notFound = {
    action() {
        BlazeLayout.render('baselayout', { main: 'notFoundTemplate' });
    },
};