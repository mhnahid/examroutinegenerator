/**
 * Created by Marvellous on 25-Mar-17.
 */
import {Mongo} from 'meteor/mongo';

export const Subjects = new Mongo.Collection('subjects');
export const Exams = new Mongo.Collection('exams');
export const Holidays = new Mongo.Collection('holidays');
